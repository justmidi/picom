#####################################################################
#
# General
#
#####################################################################

vsync = true;

# Enable DBE painting mode, intended to use with VSync to 
# (hopefully) eliminate tearing.
# Reported to have no effect, though.
dbe = false;


# Detect _NET_WM_OPACITY on client windows, useful for window 
# managers not passing _NET_WM_OPACITY of client windows to frame 
# windows.
# This prevents opacity being ignored for some apps.
# For example without this enabled my xfce4-notifyd is 100% opacity 
# no matter what.
detect-client-opacity = true;



#####################################################################
#
# Backend
#
#####################################################################

backend = "glx";
glx-no-stencil = true;

# GLX backend: Copy unmodified regions from front buffer instead 
# of redrawing them all.
# My tests with nvidia-drivers show a 10% decrease in performance 
# when the whole screen is  modified, but a 20% increase when only 
# 1/4 is.
# My tests on nouveau show terrible slowdown.
glx-copy-from-front = false;

# GLX backend: Use MESA_copy_sub_buffer to do partial screen update.
# My tests on nouveau shows a 200% performance boost when only 1/4 
# of the screen is updated.
# May break VSync and is not available on some drivers.
# Overrides --glx-copy-from-front.
#glx-use-copysubbuffermesa = true;

# GLX backend: Avoid rebinding pixmap on window damage.
# Probably could improve performance on rapid window content 
# changes, but is known to break things on some drivers (LLVMpipe).
# Recommended if it works.
#glx-no-rebind-pixmap = true;

# GLX backend: GLX buffer swap method we assume.
# Could be undefined (0), copy (1), exchange (2), 3-6, or 
# buffer-age (-1).
# undefined is the slowest and the safest, and the default value.
# copy is fastest, but may fail on some drivers,
# 2-6 are gradually slower but safer (6 is still faster than 0).
# Usually, double buffer means 2, triple buffer means 3.
# buffer-age means auto-detect using GLX_EXT_buffer_age, supported 
# by some drivers.
# Useless with --glx-use-copysubbuffermesa.
# Partially breaks --resize-damage.
# Defaults to undefined.
#glx-swap-method = "undefined";




#####################################################################
#
# Shadows
#
#####################################################################

shadow = true;
shadow-radius = 5;      # default 12
shadow-offset-x = -5;   # default -15
shadow-offset-y = -5;   # default -15
shadow-opacity = 0.5;   # default .75

log-level = "warn";

# Set if you want different colour shadows
#shadow-red = 0.0;
#shadow-green = 0.0;
#shadow-blue = 0.0;

shadow-exclude = [
    "name = 'Notifications'",
    "name *= 'compton'",
];

# Avoid drawing shadow on all shaped windows
# (see also: --detect-rounded-corners)
shadow-ignore-shaped = false;

# Detect rounded corners and treat them as rectangular when 
# --shadow-ignore-shaped is on.
detect-rounded-corners = true;



#####################################################################
#
# Opacity
#
#####################################################################

#inactive-opacity          = 0.9;
active-opacity            = 1.0;
frame-opacity             = 1.0;
inactive-opacity-override = false;

opacity-rule = [
    "90:class_g = 'dwm'",
    "97:class_g = 'kitty' && focused",
    "90:class_g = 'kitty' && !focused",
    #"90:class_g = 'kitty' && focused",
    #"80:class_g = 'kitty' && !focused",
    "100:class_g = 'mpv'",
    "100:class_g = 'zathura'",
];

# Dim inactive windows. (0.0 - 1.0)
#inactive-dim = 0.2;
# Do not let dimness adjust based on window opacity.
#inactive-dim-fixed = true;



#####################################################################
#
# Blur
#
#####################################################################

# no blur until I can get a proper backend that allows size of blur
#blur: {
#    method = "gaussian";
#    size = 12;
#
#    # Blur background of transparent windows. 
#    # Bad performance with X Render backend. 
#    # GLX backend is preferred.
#    background = true;
#    # Blur background of opaque windows with transparent frames as 
#    # well.
#    #blur-background-frame     = false;
#    # Do not let blur radius adjust based on window opacity.
#    background-fixed = false;
#
#    background-exclude = [];
#}



#####################################################################
#
# Fade
#
#####################################################################

# Fade windows during opacity changes.
fading = false;     # false until I exclude everything I want to

# The time between steps in a fade in milliseconds.
fade-delta = 2;         # default 10
# Opacity change between steps while fading in.
fade-in-step = 0.03;    # default 0.028
# Opacity change between steps while fading out.
fade-out-step = 0.03;   # default 0.03

# Fade windows in/out when opening/closing
#no-fading-openclose = true;

fade-exclude = [ ];



#####################################################################
#
# Window type
#
#####################################################################

#wintypes: {
#  tooltip = {
#    fade = true;
#    shadow = true;
#    opacity = 0.9;
#    focus = true;
#  };
#  dock = { shadow = false; }
#  dnd = { shadow = false; }
#  popup_menu = { opacity = 0.9; }
#  dropdown_menu = { opacity = 0.9; }
#};



#####################################################################
#
# XSynx
#
#####################################################################

# Use X Sync fence to sync clients' draw calls.
# Needed on nvidia-drivers with GLX backend for some users.
xrender-sync-fence = true;
